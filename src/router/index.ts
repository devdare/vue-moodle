import Vue from "vue";
import VueRouter from "vue-router";
import Dashboard from "../views/Dashboard.vue";
import Login from "../views/Login/Login.vue";
import Register from "../views/Register/Register.vue";
import CreateCourse from "../views/Courses/CreateCourse.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Dashboard",
    component: Dashboard,
    children: [
      {
        path: "/login",
        name: "Login",
        component: Login
      },
      {
        path: "/courses",
        name: "Courses",
        component: () => import("../views/Courses/Courses.vue")
      },
      {
        path: "/coursepage",
        name: "CoursePage",
        component: () => import("../views/Courses/CoursePage.vue")
      },
      {
        path: "/register",
        name: "Register",
        component: Register
      },
      {
        path: "/create",
        name: "CreateCourse",
        component: CreateCourse
      },
      {
        path: "/about",
        name: "About",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(/* webpackChunkName: "about" */ "../views/About.vue")
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
