import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";

Vue.config.productionTip = false;
import * as firebase from "firebase";
import { app } from "../src/utils/firebase";
firebase.auth().onAuthStateChanged(user => {
  console.log("Auth changed");
  console.log(user);
  store.dispatch("fetchUser", user);
});

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
