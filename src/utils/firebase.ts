import * as firebase from "firebase";

//Firebase config
type firebaseConfigInterface = {
  apiKey: string;
  authDomain: string;
  databaseURL: string;
  projectId: string;
  storageBucket: string;
  messagingSenderId: string;
  appId: string;
  measurementId: string;
};

const firebaseConfig: firebaseConfigInterface = {
  apiKey: "AIzaSyBMPRu5IqcXFo_moYJz0KDServgG7TjmEA",
  authDomain: "vue-moodle.firebaseapp.com",
  databaseURL: "https://vue-moodle.firebaseio.com",
  projectId: "vue-moodle",
  storageBucket: "vue-moodle.appspot.com",
  messagingSenderId: "391744065796",
  appId: "1:391744065796:web:95e9b908f7ce45705ac051",
  measurementId: "G-WNHMLN454D"
};
//Initialize firebase
export const app: any = firebase.initializeApp(firebaseConfig);
export const db: any = app.database();
